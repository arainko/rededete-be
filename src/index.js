const express = require("express");
const cors = require("cors");
const UserAPI = require("./api/UserAPI");
const MediaAPI = require("./api/MediaAPI");
const PostAPI = require("./api/PostAPI");
const SubredditAPI = require("./api/SubredditAPI");
const CommentAPI = require("./api/CommentAPI");
const Database = require("./persistence/Database.js");
const PostService = require("./services/PostService");
const { optionallyAuthUser } = require("./api/middlewares");
const { Some } = require("monet");
const { handleErrors } = require("./api/middlewares");
const http = require("http");
const { Protocol } = require("./util/Enums");
const { Server } = require("socket.io");

const app = express();
const server = http.createServer(app);

// --------------------------------------------
// REMEMBER TO EDIT .env!
// --------------------------------------------

const io = new Server(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  }
});

io.on(Protocol.connection, (socket) => {
  socket.on(Protocol.join, (postRoom) => {
    socket.join(postRoom);
  });
});

app.use(express.json());
app.use(cors());

app.use("/api/users", UserAPI);
app.use("/api/media", MediaAPI);
app.use("/api/post", PostAPI(io));
app.use("/api/subreddit", SubredditAPI);
app.use("/api/comment", CommentAPI(io));
app.use(handleErrors);

Database.start(() => {
  server.listen(8081, () => console.log("Listening on port 8081"));
});

module.exports = {
  io
};
