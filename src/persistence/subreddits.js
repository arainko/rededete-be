const { List } = require("monet");

const subs = [
  {
    id: 1,
    createdBy: 1,
    name: "coolpics",
    posts: []
  }
];

const nextId = () => {
  return subs
    .map(u => u.id)
    .reduce((acc, curr) => acc > curr ? acc : curr, 0) + 1;
};

module.exports = {
  getByName: name => {
    return List.fromArray(subs).filter(sub => sub.name === name).headMaybe();
  },
  getByNamePaginated: (name, offset, limit) => {
    return List
      .fromArray(subs)
      .filter(sub => sub.name.includes(name))
      .toArray()
      .slice(offset, limit);
  },
  createSubreddit: (name, user) => {
    const id = nextId();
    subs.push({
      id: nextId(),
      createdBy: user.id,
      name: name,
      posts: []
    });
    return id;
  }
};
