const dotenv = require("dotenv");
const { Client } = require("pg");

dotenv.config();

const dbConfig = {
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  database: process.env.DB_DATABASE
};

const client = new Client(dbConfig);

module.exports = {
  start: f => {
    return client.connect().then(f);
  },
  /**
   *
   * @param text
   * @param params
   * @returns {Promise<*>}
   */
  query: (text, params) => {
    return client.query(text, params);
  }
};
