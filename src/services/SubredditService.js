const SubredditRepository = require("../infrastructure/SubredditRepository");
const { Some } = require("monet");
const { DomainError } = require("../errors");

const findByName = async (name, maybeUserId) => {
  const sub = await SubredditRepository.getByName(name);
  return await sub
    .map(s => attachPrivileges(maybeUserId, s))
    .toEither(DomainError.create(404, "Subreddit not found"))
    .toPromise();
};

const findById = async (id) => {
  const sub = await SubredditRepository.getById(id);
  return await sub
    .toEither(DomainError.create(404, "Subreddit not found"))
    .toPromise();
};

const findAll = async () => {
  return await SubredditRepository.getAll();
}

const findAllContaining = async (name) => {
  return await SubredditRepository.getAllContaining(name);
}

const findSubscribedToSubs = async (userId) => {
  const subs = await SubredditRepository.getSubscribedToSubs(userId);
  return subs.map(s => attachPrivileges(Some(userId), s));
};

const subscribe = async (name, userId) => {
  const { subredditId } = await findByName(name, Some(userId));
  await SubredditRepository
    .joinSubreddit(subredditId, userId)
    .catch(() => DomainError.fail(400, "User already subscribed to the sub"));
};

const unsubscribe = async (name, userId) => {
  const { subredditId } = await findByName(name, Some(userId));
  const count = await SubredditRepository.leaveSubreddit(subredditId, userId);
  await DomainError.cond(count, 400, "User not subscribed to the sub");
};

const createSubreddit = async (userId, name, description) => {
  const subName = await SubredditRepository
    .createSubreddit(userId, name, description)
    .catch(() => DomainError.fail(400, "Subreddit with given name already exists"));
  const fetched = await findByName(subName, Some(userId));
  await SubredditRepository.joinSubreddit(fetched.subredditId, userId);
  return fetched;
};

const updateSubreddit = async (userId, name, description) => {
  const { subredditId, isPrivileged } = await findByName(name, Some(userId));
  await DomainError.cond(isPrivileged, 401, "User not authorized to edit this sub");
  await SubredditRepository.updateSubreddit(subredditId, description);
};

const attachPrivileges = (maybeUserId, sub) => {
  return {
    ...sub,
    isPrivileged: maybeUserId.exists(id => sub.modId === id)
  }
}

module.exports = {
  findByName,
  findById,
  findSubscribedToSubs,
  findAll,
  findAllContaining,
  createSubreddit,
  updateSubreddit,
  subscribe,
  unsubscribe
};
