const CommentRepository = require("../infrastructure/CommentRepository");
const PostService = require("../services/PostService");
const { Some } = require("monet");
const { DomainError } = require("../errors");

const findByPostId = async (postId, maybeUserId) => {
  const comments = await CommentRepository.getCommentsByPostId(postId);
  return comments.map(c => normalizeComment(c, maybeUserId));
};

const findById = async (commentId, maybeUserId) => {
  const comment = await CommentRepository.getCommentById(commentId);
  const liftedComment = await comment
    .toEither(DomainError.create(404, "Comment with given id was not found"))
    .toPromise();
  return normalizeComment(liftedComment, maybeUserId);
};

const deleteComment = async (commentId, userId) => {
  const comment = await findById(commentId, Some(userId));
  await DomainError.cond(comment.isPrivileged, 401, "User not authorized to delete this comment");
  await CommentRepository.deleteComment(commentId);
};

const createComment = async (content, postId, userId) => {
  await PostService.findById(Some(userId), postId);
  const id = await CommentRepository.createComment(content, postId, userId);
  return await findById(id, Some(userId));
};

const normalizeComment = (comment, maybeUserId) => {
  return {
    ...comment,
    isPrivileged: maybeUserId.exists(id => id === comment.subModId)
  };
};

module.exports = {
  findByPostId,
  findById,
  deleteComment,
  createComment
}
