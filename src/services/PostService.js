const PostRepository = require("../infrastructure/PostRepository");
const { Protocol } = require("../util/Enums");
const { Either } = require("monet");
const { Maybe } = require("monet");
const { Some } = require("monet");
const { DomainError } = require("../errors");
const { defaultValuesMap } = require("../util/MapUtils");
const { VoteStatus } = require("../util/Enums");

const findBySubsPaginated = async (maybeUserId, subNames, offset, limit, sortOrder) => {
  const posts = await PostRepository.getPostsBySubs(subNames, offset, limit, sortOrder);
  return await mergePostInfo(posts, maybeUserId);
};

const findAllContaining = async (maybeUserId, content) => {
  const posts = await PostRepository.getPostsContaining(content);
  return await mergePostInfo(posts, maybeUserId);
}

/**
 *
 * @param maybeUserId {Maybe<number>}
 * @param postId {number}
 * @returns {Promise<Post>}
 */
const findById = async (maybeUserId, postId) => {
  const maybePost = await PostRepository.getPostById(postId);
  const liftedPost = await maybePost
    .toEither(DomainError.create(404, "Post not found"))
    .toPromise();
  return await mergePostInfo([liftedPost], maybeUserId).then(res => res[0]);
};

/**
 *
 * @param userId {number}
 * @param postId {number}
 * @param status {voteStatus}
 * @returns {Promise<string>}
 */
const vote = async (userId, postId, status) => {
  const post = await findById(Some(userId), postId);
  switch (status) {
    case VoteStatus.upvoted:
      return await PostRepository
        .upvotePost(postId, userId)
        .then(() => VoteStatus.upvoted)
        .catch(() => DomainError.fail(422, `Post is already voted on (${post.voteStatus})`));
    case VoteStatus.downvoted:
      return await PostRepository
        .downvotePost(postId, userId)
        .then(() => VoteStatus.downvoted)
        .catch(() => DomainError.fail(422, `Post is already voted on (${post.voteStatus})`));
    case VoteStatus.neutral:
      return await PostRepository
        .removeVote(postId, userId)
        .then(() => VoteStatus.neutral);
    default:
      return await DomainError.fail(400, `${status} is not a valid vote state`);
  }
};

const createPost = async (userId, subId, post) => {
  const normalizedPost = preCreateNormalizePost(post);
  const insertedId = await PostRepository.createPost(userId, subId, normalizedPost);
  return await findById(Some(userId), insertedId);
};

const updatePost = async (userId, postId, post) => {
  const fetchedPost = await findById(Some(userId), postId)
  await Maybe
    .fromFalsy(fetchedPost.isPrivileged)
    .toEither(DomainError.create(401, "User not authorized to edit this post"))
    .toPromise();
  await PostRepository.updatePost(postId, post);
  return await findById(Some(userId), postId);
}

const deletePost = async (userId, postId) => {
  const post = await findById(Some(userId), postId);
  await Maybe.fromFalsy(post.isPrivileged)
    .toEither(DomainError.create(401, "User not authorized to delete this post"))
    .toPromise();
  await PostRepository.deletePost(postId);
}

/**
 *
 * @param posts {Array<*>}
 * @param maybeUserId {Maybe<number>}
 * @returns {Promise<Array<*>>}
 */
const mergePostInfo = async (posts, maybeUserId) => {
  const postIds = posts.map(post => post.id);
  const scores = await PostRepository.getPostScores(postIds);
  const statuses = await maybeUserId.cata(
    () => defaultValuesMap(postIds, "neutral"),
    id => PostRepository.getUserVoteStatuses(id, postIds)
  );
  const privileges = await maybeUserId.cata(
    () => defaultValuesMap(postIds, false),
    id => PostRepository.getPostPrivileges(id, postIds)
  );

  return posts.map(post => {
    return {
      ...post,
      imageUrl: normalizePostImageUrl(post),
      videoUrl: normalizeVideoUrl(post),
      voteStatus: statuses.get(post.id),
      isPrivileged: privileges.get(post.id),
      score: scores.get(post.id),
    };
  });
};

const preCreateNormalizePost = (post) => {
  const normalizedVideoUrl = Maybe
    .fromFalsy(post.videoUrl)
    .map(url => url.replace("/watch?v=", "/embed/"))
    .orNull();
  return {
    ...post,
    videoUrl: normalizedVideoUrl
  };
};

const normalizeVideoUrl = (post) => {
  return Maybe
    .fromFalsy(post.videoUrl)
    .map(url => url.replace("/watch?v=", "/embed/"))
    .orNull();
}

const normalizePostImageUrl = (post) => {
  return Maybe
    .fromFalsy(post.imageUrl)
    .map(url => url.includes("http")
      ? url
      : `${process.env.API_HOST}/api/media/image/${url}`
    )
    .orNull();

};

module.exports = {
  findBySubsPaginated,
  findById,
  findAllContaining,
  vote,
  createPost,
  updatePost,
  deletePost
};
