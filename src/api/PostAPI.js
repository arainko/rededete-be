const express = require("express");
const {
  authUser,
  defaultQueryParam,
  requireQueryParams,
  optionallyAuthUser,
  validated
} = require("./middlewares");
const PostService = require("../services/PostService");
const { commaSeparatedQueryParam } = require("./middlewares");
const router = express.Router();
const { Joi } = require('express-validation')

const createPostValidation = {
  body: Joi.object({
    title: Joi.string()
      .trim()
      .required()
      .max(200),
    content: Joi.string()
      .trim()
      .allow(""),
    imageUrl: Joi
      .string()
      .allow(null),
    videoUrl: Joi
      .string()
      .regex(/youtube/)
      .regex(/watch\?v=/)
      .allow(null),
    subredditId: Joi.number().required()
  })
}

module.exports = (io) => {
  router.get(
    "/",
    optionallyAuthUser,
    requireQueryParams("subIds"),
    commaSeparatedQueryParam("subIds"),
    defaultQueryParam("offset", 0),
    defaultQueryParam("limit", 50),
    defaultQueryParam("orderBy", "DESC"),
    (req, res, next) => {
      const userId = req.user.map(u => u.id);
      const {
        subIds,
        offset,
        limit,
        orderBy
      } = req.query;
      PostService.findBySubsPaginated(userId, subIds, offset, limit, orderBy)
        .then(result => res.json(result))
        .catch(next);
    }
  );

  router.get(
    "/search",
    optionallyAuthUser,
    requireQueryParams("content"),
    (req, res, next) => {
      const userId = req.user.map(u => u.id);
      const {
       content
      } = req.query;
      PostService.findAllContaining(userId, content)
        .then(result => res.json(result))
        .catch(next);
    }
  );

  router.get(
    "/:id",
    optionallyAuthUser,
    (req, res, next) => {
      const userId = req.user.map(u => u.id);
      const { id } = req.params;
      PostService.findById(userId, id)
        .then(result => res.json(result))
        .catch(next);
    }
  );

  router.post(
    "/",
    authUser(),
    validated(createPostValidation),
    (req, res, next) => {
      PostService.createPost(req.user.id, req.body.subredditId, req.body)
        .then(result => res.json(result))
        .catch(next);
    }
  );

  router.put(
    "/:id",
    authUser(),
    (req, res, next) => {
      const { id } = req.params;
      PostService.updatePost(req.user.id, id, req.body)
        .then(result => res.json(result))
        .catch(next);
    }
  );

  router.delete(
    "/:id",
    authUser(),
    (req, res, next) => {
      const { id } = req.params;
      PostService.deletePost(req.user.id, id)
        .then(() => {
          io.emit("deletePost", id);
          res.status(204).send();
        })
        .catch(next);
    }
  );

  router.put(
    "/vote/:id",
    authUser(),
    (req, res, next) => {
      const { id } = req.params;
      const { status } = req.body;
      PostService.vote(req.user.id, id, status)
        .then(value => res.json(value))
        .catch(next);
    }
  );

  return router;
};


