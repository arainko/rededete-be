const express = require("express");
const {
  authUser,
  defaultQueryParam,
  requireQueryParams,
  optionallyAuthUser,
  validated
} = require("./middlewares");
const SubredditService = require("../services/SubredditService");
const { commaSeparatedQueryParam } = require("./middlewares");
const { Joi } = require('express-validation')
const router = express.Router();

const createSubValidation = {
  body: Joi.object({
    name: Joi.string()
      .alphanum()
      .min(1)
      .max(50)
      .required(),
    description: Joi.string()
      .allow("")
      .max(200)
      .required()
  })
};

const updateSubValidation = {
  body: Joi.object({
    description: Joi.string()
      .empty("")
      .max(200)
  })
}

router.get(
  "/",
  (req, res, next) => {
    SubredditService.findAll()
      .then(result => res.send(result))
      .catch(next);
  }
);

router.get(
  "/search",
  requireQueryParams("name"),
  (req, res, next) => {
    const { name } = req.query;
    SubredditService.findAllContaining(name)
      .then(result => res.send(result))
      .catch(next);
  }
);

router.get(
  "/me",
  authUser(),
  (req, res, next) => {
    SubredditService.findSubscribedToSubs(req.user.id)
      .then(result => res.send(result))
      .catch(next);
  }
);

router.get(
  "/name/:name",
  optionallyAuthUser,
  (req, res, next) => {
    const { name } = req.params;
    const userId = req.user.map(u => u.id);
    SubredditService.findByName(name, userId)
      .then(result => res.json(result))
      .catch(next);
  }
);

router.post(
  "/",
  authUser(),
  validated(createSubValidation),
  (req, res, next) => {
    const { name, description } = req.body;
    SubredditService.createSubreddit(req.user.id, name, description)
      .then(result => res.json(result))
      .catch(next);
  }
);

router.put(
  "/name/:name",
  authUser(),
  validated(updateSubValidation),
  (req, res, next) => {
    const { description } = req.body;
    const { name } = req.params;
    SubredditService.updateSubreddit(req.user.id, name, description)
      .then(() => res.status(204).send())
      .catch(next);
  }
);

router.post(
  "/subscribe/:name",
  authUser(),
  (req, res, next) => {
    const { name } = req.params;
    SubredditService.subscribe(name, req.user.id)
      .then(() => res.status(204).send())
      .catch(next)
  }
);

router.post(
  "/unsubscribe/:name",
  authUser(),
  (req, res, next) => {
    const { name } = req.params;
    SubredditService.unsubscribe(name, req.user.id)
      .then(() => res.status(204).send())
      .catch(next)
  }
);

module.exports = router;
