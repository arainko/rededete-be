const express = require("express");
const path = require("path");
const fs = require("fs");
const multer = require("multer");
const { DomainError } = require("../errors");

const router = express.Router();

const storage = multer.diskStorage(
  {
    destination: "images/",
    filename: (req, file, cb) => {
      const timestamp = new Date().valueOf();
      cb(null, `${timestamp}_${file.originalname}`);
    }
  }
)

const uploader = multer({
  storage,
  fileFilter: (req, file, cb) => {
    if (file.mimetype === "image/png" || file.mimetype === "image/jpg" || file.mimetype === "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(DomainError.create(400,'Only .png, .jpg and .jpeg format allowed!'));
    }
  }
});

router.post("/image",
  uploader.single("image"),
  async (req, res) => {
    res.json({ filename: req.file.filename });
  }
);

router.get("/image/:filename",
  async (req, res, next) => {
    const { filename } = req.params;
    const directory = path.resolve();
    const fullPath = path.join(directory, "images/" + filename);
    fs.promises.access(fullPath)
      .then(() => res.sendFile(fullPath))
      .catch(() => DomainError.handled(404, "File not found", next));
  }
);

module.exports = router;
