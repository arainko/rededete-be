const express = require("express");
const { authUser, validated } = require("./middlewares");
const UserRepository = require("../infrastructure/UserRepository");
const { DomainError } = require("../errors");
const { Joi } = require('express-validation');

const router = express.Router();

const registrationValidation = {
  body: Joi.object({
    username: Joi.string()
      .alphanum()
      .trim()
      .max(50)
      .required(),
    email: Joi.string()
      .email()
      .required(),
    password: Joi.string()
      .required()
      .alphanum()
      .trim()
      .min(5),
    repeatedPassword: Joi.string()
      .required()
      .alphanum()
      .trim()
      .min(5)
  })
}

const credentialsValidation = {
  body: Joi.object({
    email: Joi.string()
      .email()
      .required(),
    password: Joi.string()
      .required()
      .alphanum()
      .trim()
      .min(5),
    repeatedPassword: Joi.string()
      .required()
      .alphanum()
      .trim()
      .min(5)
  })
};

router.get("/me",
  authUser(),
  async (req, res) => {
    res.json(req.user);
  }
);

router.put("/me/password",
  authUser(),
  validated(credentialsValidation),
  (req, res, next) => {
    const { email, password, repeatedPassword } = req.body;
    const { id } = req.user;
    UserRepository.updatePassword(id, email, password, repeatedPassword)
      .then(() => res.status(204).send())
      .catch(next);
  }
);

router.post("/register",
  validated(registrationValidation),
  (req, res, next) => {
    const { username, email, password, repeatedPassword } = req.body
    UserRepository.register(username, email, password, repeatedPassword)
      .then(user => res.json(user))
      .catch(next);
  }
);

module.exports = router;
