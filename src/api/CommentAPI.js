const express = require("express");
const { optionallyAuthUser } = require("./middlewares");
const router = express.Router();
const CommentService = require("../services/CommentService");
const { Protocol } = require("../util/Enums");
const { authUser, validated } = require("./middlewares");
const { Joi } = require('express-validation')

const createCommentValidation = {
  body: Joi.object({
    content: Joi.string().trim().max(200).required()
  })
}

module.exports = (io) => {
  router.get(
    "/:postId",
    optionallyAuthUser,
    (req, res, next) => {
      const maybeUserId = req.user.map(u => u.id);
      const { postId } = req.params;
      CommentService.findByPostId(postId, maybeUserId)
        .then(result => res.json(result))
        .catch(next);
    }
  );

  router.post(
    "/:postId",
    authUser(),
    validated(createCommentValidation),
    (req, res, next) => {
      const { postId } = req.params;
      const { id } = req.user;
      const { content } = req.body;
      CommentService.createComment(content, postId, id)
        .then(result => {
          io.in(Protocol.postRoom(postId)).emit(Protocol.addComment, result);
          res.json(result);
        })
        .catch(next);
    }
  );

  router.delete(
    "/:postId/:commentId",
    authUser(),
    (req, res, next) => {
      const {
        postId,
        commentId
      } = req.params;
      const { id } = req.user;
      CommentService.deleteComment(commentId, id)
        .then(() => {
          io.in(Protocol.postRoom(postId)).emit(Protocol.deleteComment, commentId);
          res.status(204).send();
        })
        .catch(next);
    }
  );

  return router;
};
