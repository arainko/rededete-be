const passport = require("passport");
const { handleError } = require("../errors");
const { DomainError } = require("../errors");
const { Maybe } = require("monet");
const { BasicStrategy } = require("passport-http");
const { login } = require("../infrastructure/UserRepository");
const {validate, ValidationError } = require("express-validation");

passport.use(
  new BasicStrategy(
    async (username, password, done) => {
      const res = await login(username, password);
      return res.cata(
        () => done(null, false),
        user => done(null, user)
      );
    }
  )
);

const defaultQueryParam = (name, value) => (req, res, next) => {
  Maybe.fromFalsy(req.query[name]).orElseRun(() => req.query[name] = value);
  next();
};

const verifyQueryParams = (...requiredParams) => (req, res, next) => {
  const params = Object.keys(req.query);
  const missingParams = requiredParams
    .map(param => {
      return {
        param: param,
        present: params.includes(param)
      };
    }).filter(({ present }) => !present).map(({ param }) => param);
  missingParams.length === 0
    ? next()
    : DomainError.handled(400, `Query params missing: ${missingParams.join(", ")}`, next);
};

const validated = (schema) => validate(schema, { keyByField: true }, {});

const handleDomainErrors = (err, req, res, next) => {
  if (err instanceof DomainError) {
    handleError(err, res);
  } else if (err instanceof ValidationError) {
    const onlyMessages = err
      .details
      .flatMap(err => Object.values(err))
      .map(msg => msg.replace("\"", "").replace("\"", "").replace(/([a-z])([A-Z])/g, '$1 $2'));
    res.status(400).json({
      status: "validation error",
      statusCode: 400,
      message: onlyMessages.join(", ").toLowerCase()
    });
  } else {
    console.log(err);
    res.status(500).json({
      status: "defect",
      statusCode: 500,
      message: "Internal server error"
    });
  }
};

const optionallyAuthUser = (req, res, next) => {
  passport.authenticate("basic", (err, user, info) => {
    req.user = Maybe.fromFalsy(user);
    next();
  })(req, res, next);
};

const commaSeparatedQueryParam = (name) => (req, res, next) => {
  req.query[name] = req.query[name].split(",");
  next();
};

module.exports = {
  authUser: () => passport.authenticate("basic", { session: false }),
  optionallyAuthUser,
  requireQueryParams: verifyQueryParams,
  defaultQueryParam: defaultQueryParam,
  handleErrors: handleDomainErrors,
  commaSeparatedQueryParam,
  validated
};
