const { Maybe } = require("monet");

class DomainError extends Error {
  constructor (statusCode, message) {
    super();
    this.statusCode = statusCode;
    this.message = message;
  }

  static create (statusCode, message) {
    return new DomainError(statusCode, message);
  }

  static fail (statusCode, message) {
    return Promise.reject(DomainError.create(statusCode, message));
  }

  static cond (condition, statusCode, message) {
    return Maybe.fromFalsy(condition)
      .toEither(DomainError.create(statusCode, message))
      .toPromise();
  }

  static handled (statusCode, message, handler) {
    handler(new DomainError(statusCode, message));
  }
}

const handleError = (err, res) => {
  const {
    statusCode,
    message
  } = err;
  res.status(statusCode).json({
    status: "failure",
    statusCode,
    message
  });
};

module.exports = {
  handleError,
  DomainError
};
