module.exports = {
  VoteStatus: {
    upvoted: "upvoted",
    downvoted: "downvoted",
    neutral: "neutral"
  },
  Protocol: {
    connection: "connection",
    addComment: "addComment",
    deleteComment: "deleteComment",
    deletePost: "deletePost",
    join: "join",
    postRoom: (postId) => `post-${postId}`
  }
}
