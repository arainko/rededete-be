const defaultValuesMap = (values, defaultValue) => {
  return values.reduce((acc, curr) => acc.set(curr, defaultValue), new Map());
}

module.exports = {
  defaultValuesMap
}
