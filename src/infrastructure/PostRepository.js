const { List } = require("monet");
const { query } = require("../persistence/Database.js");
const format = require("pg-format");
const { defaultValuesMap } = require("../util/MapUtils");

const createPost = async (userId, subId, {
  title,
  content,
  imageUrl,
  videoUrl
}) => {
  const sql = `
      INSERT INTO post (title, content, image_path, video_url, creation_date, subreddit_id, user_id)
      VALUES ($1, $2, $3, $4, $5, $6, $7)
      RETURNING id
  `;
  const timestamp = new Date();
  const params = [title, content, imageUrl, videoUrl, timestamp, subId, userId];
  return query(sql, params).then(rs => rs.rows[0].id);
};

const updatePost = async (id, {
  title,
  content,
  imageUrl,
  videoUrl
}) => {
  const sql = `
      UPDATE post
      SET title      = $1,
          content    = $2,
          image_path = $3,
          video_url  = $4
      WHERE id = $5
  `;
  const params = [title, content, imageUrl, videoUrl, id];
  const rs = await query(sql, params);
  return await rs.rows[0];
};

const deletePost = async id => {
  const sql = `
      DELETE
      FROM post
      WHERE id = $1
  `;
  const params = [id];
  return query(sql, params);
};

const upvotePost = async (id, userId) => {
  const sql = "INSERT INTO post_vote (vote, user_id, post_id) VALUES (1, $1, $2)";
  const params = [userId, id];
  await query(sql, params);
};

const downvotePost = async (id, userId) => {
  const sql = "INSERT INTO post_vote (vote, user_id, post_id) VALUES (-1, $1, $2)";
  const params = [userId, id];
  await query(sql, params);
};

const removeVote = async (id, userId) => {
  const sql = "DELETE FROM post_vote WHERE user_id = $1 AND post_id = $2";
  const params = [userId, id];
  await query(sql, params);
};

/**
 *
 * @param ids {Array<number>}
 * @returns {Promise<Map<number, number>>}
 */
const getPostScores = async (ids) => {
  const sql = format(`
      SELECT post_id as "id", SUM(vote)::INTEGER as score
      FROM post_vote
      WHERE post_id IN (%L)
      GROUP BY post_id
  `, ids.length > 0 ? ids : null);
  const fetched = await query(sql, []).then(res => res.rows);
  const resultMap = ids.reduce((acc, curr) => acc.set(curr, 0), new Map());
  return fetched.reduce((acc, curr) => acc.set(curr.id, curr.score), resultMap);
};

/**
 *
 * @param userId {number}
 * @param ids {Array<number>}
 * @returns {Promise<Map<number, string>>}
 */
const getUserVoteStatuses = async (userId, ids) => {
  const sql = format(`
      SELECT post_id as "id",
             CASE
                 WHEN SUM(vote) = 0 THEN 'neutral'
                 WHEN SUM(vote) > 0 THEN 'upvoted'
                 WHEN SUM(vote) < 0 THEN 'downvoted'
                 ELSE 'neutral'
                 END AS status
      FROM post_vote pv
      WHERE pv.user_id = $1
        AND pv.post_id IN (%L)
      GROUP BY pv.post_id
  `, ids.length > 0 ? ids : null);
  const params = [userId];
  const resultMap = defaultValuesMap(ids, "neutral");
  const fetched = await query(sql, params).then(res => res.rows);
  return fetched.reduce((acc, curr) => acc.set(curr.id, curr.status), resultMap);
};

/**
 *
 * @param userId {number}
 * @param postIds {Array<number>}
 * @returns {Promise<Map<number, boolean>>}
 */
const getPostPrivileges = async (userId, postIds) => {
  const sql = format(`
      SELECT post.id                                as "id",
             (post.user_id = $1 OR sub.mod_id = $2) as "isPrivileged"
      FROM post
               JOIN subreddit sub on sub.id = post.subreddit_id
      WHERE post.id IN (%L)
  `, postIds.length > 0 ? postIds : null);
  const params = [userId, userId];
  return await query(sql, params)
    .then(res => res.rows.reduce((acc, curr) => acc.set(curr.id, curr.isPrivileged), new Map()));
};

const getPostsBySubs = async (subNames, offset, limit, sortOrder) => {
  const sortedBy = sortOrder === "ASC" ? "ASC" : "DESC";
  const sql = format(`
      SELECT post.id,
             title,
             content,
             image_path as "imageUrl",
             video_url as "videoUrl",
             creation_date as "creationDate",
             subreddit_id as "subredditId",
             user_id as "userId",
             nickname as "userNickname",
             name as "subName"
      FROM post
               JOIN subreddit s on s.id = post.subreddit_id
               JOIN reddit_user ru on ru.id = post.user_id
      WHERE s.name IN (%L)
      ORDER BY post.creation_date ${sortedBy}
      OFFSET $1 LIMIT $2
  `, subNames.length > 0 ? subNames : null);
  const params = [offset, limit];
  return await query(sql, params).then(res => res.rows);
};

const getPostsContaining = async (content) => {
  const sql = `
      SELECT post.id,
             title,
             content,
             image_path as "imageUrl",
             video_url as "videoUrl",
             creation_date as "creationDate",
             subreddit_id as "subredditId",
             user_id as "userId",
             nickname as "userNickname",
             name as "subName"
      FROM post
               JOIN subreddit s on s.id = post.subreddit_id
               JOIN reddit_user ru on ru.id = post.user_id
      WHERE post.content LIKE '%' || $1 || '%'
      ORDER BY post.creation_date DESC
      
  `;
  const params = [content];
  return await query(sql, params).then(res => res.rows);
};

/**
 *
 * @param postId {number}
 * @returns {Promise<Maybe<Post>>}
 */
const getPostById = async (postId) => {
  const sql = `
      SELECT post.id,
             title,
             content,
             image_path as "imageUrl",
             video_url as "videoUrl",
             creation_date as "creationDate",
             subreddit_id as "subredditId",
             user_id as "userId",
             nickname as "userNickname",
             name as "subName"
      FROM post
               JOIN subreddit s on s.id = post.subreddit_id
               JOIN reddit_user ru on ru.id = post.user_id
      WHERE post.id = $1
      LIMIT 1
  `;
  const params = [postId];
  return await query(sql, params).then(res => List.fromArray(res.rows).headMaybe());
};

module.exports = {
  getPostsBySubs,
  getPostById,
  getPostPrivileges,
  getUserVoteStatuses,
  getPostScores,
  getPostsContaining,
  createPost,
  updatePost,
  deletePost,
  removeVote,
  upvotePost,
  downvotePost
};
