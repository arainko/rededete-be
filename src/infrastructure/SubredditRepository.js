const { List } = require("monet");
const { query } = require("../persistence/Database.js");

/**
 *
 * @param userId {number}
 * @param name {string}
 * @param description {string}
 * @returns {Promise<number>}
 */
const createSubreddit = async (userId, name, description) => {
  const sql = `
      INSERT INTO subreddit (name, description, mod_id)
      VALUES ($1, $2, $3)
      RETURNING id
  `;
  const params = [name.toLowerCase(), description, userId];
  return await query(sql, params).then(() => name.toLowerCase());
};

const updateSubreddit = async (subId, description) => {
  const sql = `
      UPDATE subreddit
      SET description = $1
      WHERE id = $2
  `;
  const params = [description, subId];
  await query(sql, params);
};

/**
 *
 * @param name {string}
 * @returns {Promise<Maybe<Subreddit>>}
 */
const getByName = async (name) => {
  const sql = `
      SELECT id as "subredditId", name as "subName", description as "subDescription", mod_id::INTEGER as "modId"
      FROM subreddit
      WHERE name = $1
  `;
  const params = [name.toLowerCase()];
  return await query(sql, params).then(res => List.fromArray(res.rows).headMaybe());
};

const getById = async (id) => {
  const sql = `
      SELECT id as "subredditId", name as "subName", description as "subDescription", mod_id::INTEGER as "modId"
      FROM subreddit
      WHERE id = $1
  `;
  const params = [id];
  return await query(sql, params).then(res => List.fromArray(res.rows).headMaybe());
};

const getAll = async () => {
  const sql = `
      SELECT id as "subredditId", name as "subName", description as "subDescription", mod_id::INTEGER as "modId"
      FROM subreddit
  `;
  const params = [];
  return await query(sql, params).then(res => res.rows);
};

const getAllContaining = async (name) => {
  const sql = `
      SELECT 
             id as "subredditId",
             name as "subName",
             description as "subDescription",
             mod_id::INTEGER as "modId"
      FROM subreddit
      WHERE name LIKE '%' || $1 || '%'
  `;
  const params = [name];
  return await query(sql, params).then(res => res.rows);
}

const joinSubreddit = async (subId, userId) => {
  const sql = `
      INSERT INTO subreddit_user (user_id, subreddit_id)
      VALUES ($1, $2)
  `;
  const params = [userId, subId];
  return await query(sql, params).then(res => res.rowCount);
};

const leaveSubreddit = async (subId, userId) => {
  const sql = `
      DELETE
      FROM subreddit_user
      WHERE subreddit_id = $1
        AND user_id = $2
  `;
  const params = [subId, userId];
  return await query(sql, params).then(res => res.rowCount);
};

const getSubscribedToSubs = async (userId) => {
  const sql = `
      SELECT subreddit.id as "subredditId", name as "subName", description as "subDescription", mod_id::INTEGER as "modId"
      FROM subreddit
               JOIN subreddit_user su on subreddit.id = su.subreddit_id
      WHERE su.user_id = $1
  `;
  const params = [userId];
  return await query(sql, params).then(res => res.rows);
};

module.exports = {
  createSubreddit,
  updateSubreddit,
  getByName,
  getById,
  getAll,
  getAllContaining,
  getSubscribedToSubs,
  joinSubreddit,
  leaveSubreddit
};
