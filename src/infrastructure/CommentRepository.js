const { List } = require("monet");
const { query } = require("../persistence/Database.js");
const format = require("pg-format");
const { defaultValuesMap } = require("../util/MapUtils");

const getCommentsByPostId = async (postId) => {
  const sql = `
      SELECT comment.id      as "id",
             comment.content as "content",
             comment.user_id as "userId",
             post_id         as "postId",
             mod_id::INTEGER as "subModId",
             ru.nickname     as "user"
      FROM comment
               JOIN post p on p.id = comment.post_id
               JOIN subreddit s on s.id = p.subreddit_id
               JOIN reddit_user ru on ru.id = comment.user_id
      WHERE post_id = $1
      ORDER BY comment.id DESC
  `;
  const params = [postId];
  return await query(sql, params).then(res => res.rows);
};

const getCommentById = async (commentId) => {
  const sql = `
      SELECT comment.id      as "id",
             comment.content as "content",
             comment.user_id as "userId",
             post_id         as "postId",
             mod_id::INTEGER as "subModId",
             ru.nickname     as "user"
      FROM comment
               JOIN post p on p.id = comment.post_id
               JOIN subreddit s on s.id = p.subreddit_id
               JOIN reddit_user ru on ru.id = comment.user_id
      WHERE comment.id = $1
  `;
  const params = [commentId];
  return await query(sql, params).then(res => List.fromArray(res.rows).headMaybe());
};

const createComment = async (content, postId, userId) => {
  const sql = `
      INSERT INTO comment(content, post_id, user_id)
      VALUES ($1, $2, $3)
      RETURNING id;
  `;
  const params = [content, postId, userId];
  return await query(sql, params).then(res => res.rows[0].id);
};

const deleteComment = async (commentId) => {
  const sql = `
      DELETE
      FROM comment
      WHERE id = $1
  `;
  const params = [commentId];
  return await query(sql, params).then(res => res.rowCount);
};

module.exports = {
  getCommentsByPostId,
  getCommentById,
  createComment,
  deleteComment
};
