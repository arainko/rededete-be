const { DomainError } = require("../errors");
const {
  List,
  Either
} = require("monet");
const { query } = require("../persistence/Database.js");

const insertUser = async user => {
  const sql = "INSERT INTO reddit_user(nickname, password, email) VALUES ($1, $2, $3) RETURNING id";
  const params = [user.username, user.password, user.email];
  const result = await query(sql, params);
  return result.rows[0].id;
};

const updateUserCredentials = async (id, credentials) => {
  const sql = "UPDATE reddit_user SET password = $1, email = $2 WHERE id = $3 ";
  const params = [credentials.password, credentials.email, id];
  const result = await query(sql, params);
  return result.rows[0];
};

const getById = async id => {
  const sql = "SELECT * FROM reddit_user WHERE id = $1";
  const params = [id];
  const result = await query(sql, params);
  return List.fromArray(result.rows).headMaybe();
};

const getByCredentials = async (email, password) => {
  const sql = "SELECT * FROM reddit_user WHERE email = $1 AND password = $2";
  const params = [email, password];
  const result = await query(sql, params);
  return List.fromArray(result.rows).headMaybe();
};

const existsByEmail = async email => {
  const sql = "SELECT * FROM reddit_user WHERE email = $1";
  const params = [email];
  const result = await query(sql, params);
  return List.fromArray(result.rows).headMaybe().isSome();
};

module.exports = {
  existsByEmail,
  login: async (email, password) => {
    return await getByCredentials(email, password);
  },
  register: async (username, email, password, repeatedPassword) => {
    const isAlreadyExisting = await existsByEmail(email);
    await DomainError.cond(!isAlreadyExisting, 422, "User already registered!");
    await DomainError.cond(password === repeatedPassword, 400, "Passwords don't match!");
    const insertedId = await insertUser({
      username,
      email,
      password
    });
    return {
      id: insertedId,
      username,
      email,
      password
    };
  },
  updatePassword: async (id, email, password, repeatedPassword) => {
    await DomainError.cond(password === repeatedPassword, 400, "Passwords don't match!");
    return updateUserCredentials(id, {
      email,
      password
    });
  },
  getById
};
